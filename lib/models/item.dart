import 'dart:convert';

Item itemFromJson(String str) => Item.fromJson(json.decode(str));

String itemToJson(Item data) => json.encode(data.toJson());

class Item {
  List<Listing> listing;
  Status status;

  Item({
    required this.listing,
    required this.status,
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        listing:
            List<Listing>.from(json["listing"].map((x) => Listing.fromJson(x))),
        status: Status.fromJson(json["status"]),
      );

  Map<String, dynamic> toJson() => {
        "listing": List<dynamic>.from(listing.map((x) => x.toJson())),
        "status": status.toJson(),
      };
}

class Listing {
  String id;
  String listName;
  String distance;

  Listing({
    required this.id,
    required this.listName,
    required this.distance,
  });

  factory Listing.fromJson(Map<String, dynamic> json) => Listing(
        id: json["id"],
        listName: json["list_name"],
        distance: json["distance"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "list_name": listName,
        "distance": distance,
      };
}

class Status {
  int code;
  String message;

  Status({
    required this.code,
    required this.message,
  });

  factory Status.fromJson(Map<String, dynamic> json) => Status(
        code: json["code"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
      };
}
