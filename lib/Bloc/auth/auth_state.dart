part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  @override
  List<Object?> get props => [];
}

class AuthInitialState extends AuthState {}

class AuthLoadingState extends AuthState {}

class LoginSuccessState extends AuthState {
  final User user;

  LoginSuccessState({required this.user});
  @override
  List<Object?> get props => [user];
}

class LoginErrorState extends AuthState {
  final String error;

  LoginErrorState({required this.error});
  @override
  List<Object?> get props => [error];
}
