import 'dart:convert';

import 'package:advisory_app/models/user.dart';
import 'package:advisory_app/provider/http_provider.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  // final HttpProvider httpProvider;

  AuthBloc() : super(AuthInitialState()) {
    on<LoginPressed>((event, emit) {
      return _mapLoginState(event, emit);
    });
  }

  void _mapLoginState(event, emit) async {
    emit(AuthLoadingState());

    var dataReturn = await HttpProvider.postHttp(
        "login", {'email': event.email, 'password': event.password});

    Map<String, dynamic> data = jsonDecode(dataReturn);

    var status = data['status']['code'];

    debugPrint("new token: $data");

    if (status == 200 || status == 201) {
      SharedPreferences prefs = await SharedPreferences.getInstance();

      prefs.setString('id', data['id'].toString());
      prefs.setString('token', data['token'].toString());
      emit(LoginSuccessState(user: userFromJson(dataReturn)));
    } else {
      var errorMessage = data['status']['message'];
      emit(LoginErrorState(error: errorMessage));
    }
  }
}
