part of 'item_bloc.dart';

abstract class ItemState extends Equatable {
  @override
  List<Object?> get props => [];
}

class ItemInitialState extends ItemState {}

class ItemLoadingState extends ItemState {}

class ItemSuccessState extends ItemState {
  final Item item;

  ItemSuccessState({required this.item});
  @override
  List<Object?> get props => [item];
}

class ItemErrorState extends ItemState {
  final String error;

  ItemErrorState({required this.error});
  @override
  List<Object?> get props => [error];
}
