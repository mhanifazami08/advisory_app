import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../../models/item.dart';
import '../../provider/http_provider.dart';

part 'item_event.dart';
part 'item_state.dart';

class ItemBloc extends Bloc<ItemEvent, ItemState> {
  ItemBloc() : super(ItemInitialState()) {
    on<GetItem>((event, emit) {
      return _mapGetItemState(event, emit);
    });
  }

  void _mapGetItemState(event, emit) async {
    emit(ItemLoadingState());

    var dataReturn = await HttpProvider.getHttp(
        "listing", {'id': event.id, 'token': event.token});

    Map<String, dynamic> data = jsonDecode(dataReturn);

    var status = data['status']['code'];

    debugPrint(dataReturn);

    debugPrint(status.toString());

    if (status == 200 || status == 201) {
      emit(ItemSuccessState(item: itemFromJson(dataReturn)));
    } else {
      var errorMessage = data['status']['message'];
      emit(ItemErrorState(error: errorMessage));
    }
  }
}
