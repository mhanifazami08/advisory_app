part of 'item_bloc.dart';

abstract class ItemEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetItem extends ItemEvent {
  final String id;
  final String token;

  GetItem({required this.id, required this.token});

  @override
  List<Object> get props => [id, token];
}
