import 'package:flutter/material.dart';
// ignore_for_file: prefer_const_constructors

class TextFieldWidget extends StatelessWidget {
  final dynamic textController;
  final String hintText;
  final Icon icon;
  final bool obscureText;

  const TextFieldWidget({
    super.key,
    required this.textController,
    required this.hintText,
    required this.icon,
    required this.obscureText,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 5),
      child: TextFormField(
        controller: textController,
        obscureText: obscureText,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade400),
          ),
          fillColor: Colors.grey.shade200,
          hintText: hintText,
          hintStyle: TextStyle(color: Colors.grey[500]),
          icon: icon,
          filled: true,
        ),
      ),
    );
  }
}
