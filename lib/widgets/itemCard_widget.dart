import 'package:advisory_app/models/item.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
// ignore_for_file: prefer_const_constructors

class ItemCardWidget extends StatelessWidget {
  final Listing item;
  const ItemCardWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8,
      shadowColor: Colors.black,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
      child: Container(
          decoration: BoxDecoration(
            color: Colors.white38,
          ),
          child: ListTile(
              onTap: () {
                Fluttertoast.showToast(
                  msg: "${item.listName}, ${item.distance}km",
                  toastLength: Toast.LENGTH_SHORT,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: 16.0,
                );
              },
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 25.0, vertical: 10.0),
              title: Text(
                item.listName,
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
              subtitle: Row(
                children: [
                  Icon(Icons.location_pin, color: Color(0xFF926A24)),
                  Text(" ${item.distance}km",
                      style: TextStyle(color: Colors.black))
                ],
              ),
              trailing: Icon(Icons.keyboard_arrow_right,
                  color: Color(0xFF926A24), size: 30.0))),
    );
  }
}
