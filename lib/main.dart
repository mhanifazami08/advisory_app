import 'package:advisory_app/provider/http_provider.dart';
import 'package:advisory_app/screens/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'Bloc/auth/auth_bloc.dart';
import 'Bloc/item/item_bloc.dart';
import 'screens/login_page.dart';

void main() {
  runApp(const MyApp());
}

final HttpProvider httpProvider = HttpProvider();

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthBloc>(create: (context) => AuthBloc()),
        BlocProvider<ItemBloc>(create: (context) => ItemBloc()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            primarySwatch: const MaterialColor(0xFF926A24, {
          50: Color(0xFF926A24),
          100: Color(0xFF926A24),
          200: Color(0xFF926A24),
          300: Color(0xFF926A24),
          400: Color(0xFF926A24),
          500: Color(0xFF926A24),
          600: Color(0xFF926A24),
          700: Color(0xFF926A24),
          800: Color(0xFF926A24),
          900: Color(0xFF926A24),
        })),
        debugShowCheckedModeBanner: false,
        home: const LoginPage(),
      ),
    );
  }
}
