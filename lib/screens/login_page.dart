import 'dart:convert';

import 'package:advisory_app/Bloc/auth/auth_bloc.dart';
import 'package:advisory_app/screens/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../widgets/textfield_widget.dart';
import 'profile_page.dart';
// ignore_for_file: prefer_const_constructors

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late AuthBloc _authBloc;
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  // String id = "";
  // String token = "";
  // String email = "";
  // String picture = "";

  @override
  void initState() {
    getUserId();
    super.initState();
    _authBloc = BlocProvider.of<AuthBloc>(context);
  }

  getUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    // id = prefs.getString('id')!;
    // token = prefs.getString('token')!;

    // email = prefs.getString('email')!;
    // picture = prefs.getString('picture')!;

    if (prefs.containsKey('id') == true && prefs.containsKey('token') == true) {
      // ignore: use_build_context_synchronously
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    } else if (prefs.containsKey('email') == true &&
        prefs.containsKey('picture')) {
      // ignore: use_build_context_synchronously
      Future.delayed(const Duration(seconds: 1), () {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => ProfilePage()));
      });
    }
  }

  _loginFb() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final LoginResult loginResult = await FacebookAuth.instance.login();

    if (loginResult.status == LoginStatus.success) {
      final userInfo =
          await FacebookAuth.instance.getUserData(fields: "email, picture");

      var email = userInfo['email'];
      var picture = userInfo['picture']['data']['url'];

      prefs.setString('email', email);
      prefs.setString('picture', picture);

      debugPrint("email: $email");
      debugPrint("picture: $picture");

      // ignore: use_build_context_synchronously
      Future.delayed(const Duration(seconds: 1), () {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => ProfilePage()));
      });
    } else {
      debugPrint('ResultStatus: ${loginResult.status}');
      debugPrint('Message: ${loginResult.message}');

      Fluttertoast.showToast(
        msg: loginResult.message.toString(),
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is LoginSuccessState) {
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => HomePage()));
            Fluttertoast.showToast(
              msg: state.user.status.message,
              toastLength: Toast.LENGTH_SHORT,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.black,
              textColor: Colors.white,
              fontSize: 16.0,
            );
          } else if (state is LoginErrorState) {
            Fluttertoast.showToast(
              msg: state.error,
              toastLength: Toast.LENGTH_SHORT,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.black,
              textColor: Colors.white,
              fontSize: 16.0,
            );
          }
        },
        child: BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            if (state is AuthLoadingState) {
              return Center(child: CircularProgressIndicator());
            } else {
              return SafeArea(
                child: Center(
                  child: Column(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Image.asset(
                          'assets/images/advisory_logo.png',
                        ),
                      ),
                      Expanded(
                        flex: 6,
                        child: _content(context),
                      ),
                    ],
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  Widget _content(context) {
    return Column(
      children: [
        TextFieldWidget(
          textController: emailController,
          hintText: 'Email',
          icon: Icon(Icons.person),
          obscureText: false,
        ),
        TextFieldWidget(
          textController: passwordController,
          hintText: 'Password',
          icon: Icon(Icons.key),
          obscureText: true,
        ),
        SizedBox(height: 30),
        GestureDetector(
          onTap: () {
            _authBloc.add(LoginPressed(
                email: emailController.text,
                password: passwordController.text));
          },
          child: Container(
              padding: const EdgeInsets.all(25),
              margin: const EdgeInsets.symmetric(horizontal: 25),
              decoration: BoxDecoration(
                color: Color(0xFF926A24),
                borderRadius: BorderRadius.circular(8),
              ),
              child: const Center(
                child: Text(
                  "Sign In",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              )),
        ),
        SizedBox(
          height: 50,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25),
          child: Row(
            children: [
              Expanded(
                child: Divider(
                  thickness: 0.6,
                  color: Colors.grey[700],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  'or continue with',
                  style: TextStyle(
                    color: Colors.grey[700],
                  ),
                ),
              ),
              Expanded(
                child: Divider(
                  thickness: 0.6,
                  color: Colors.grey[700],
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 50),
        GestureDetector(
          onTap: () => _loginFb(),
          child: Image.asset(
            'assets/images/facebook.png',
            height: 60,
          ),
        ),
      ],
    );
  }
}
