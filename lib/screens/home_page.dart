import 'package:advisory_app/Bloc/item/item_bloc.dart';
import 'package:advisory_app/screens/login_page.dart';
import 'package:advisory_app/widgets/itemCard_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
// ignore_for_file: prefer_const_constructors

import '../models/item.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Listing> list = [];
  String id = "";
  String token = "";
  bool fb = false;

  @override
  void initState() {
    getUserId();
    super.initState();
  }

  getUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      id = prefs.getString('id')!;
      token = prefs.getString('token')!;

      context.read<ItemBloc>().add(GetItem(id: id, token: token));
    });
  }

  logout(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('id');
    prefs.remove('token');

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LoginPage()));
  }

  confirmLogoutDialog(BuildContext ctx) {
    showDialog(
        context: ctx,
        builder: (ctx) {
          return AlertDialog(
            title: const Text("Confirm logout?"),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(ctx);
                  },
                  child: const Text(
                    'Cancel',
                    style: TextStyle(color: Colors.grey),
                  )),
              TextButton(
                  onPressed: () => logout(ctx), child: const Text('Confirm')),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF926A24),
      appBar: AppBar(
        centerTitle: true,
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: const Text('Listing'),
        actions: [
          IconButton(
              onPressed: () => confirmLogoutDialog(context),
              icon: Icon(Icons.logout)),
          SizedBox(
            width: 5,
          )
        ],
      ),
      body: SafeArea(
        child: WillPopScope(
          onWillPop: () => confirmLogoutDialog(context),
          child: BlocListener<ItemBloc, ItemState>(
            listener: (context, state) {
              if (state is ItemErrorState) {
                Fluttertoast.showToast(
                  msg: state.error,
                  toastLength: Toast.LENGTH_SHORT,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: 16.0,
                );
              }
            },
            child: BlocBuilder<ItemBloc, ItemState>(
              builder: (context, state) {
                if (state is ItemLoadingState) {
                  return Center(child: CircularProgressIndicator());
                } else if (state is ItemSuccessState) {
                  list = state.item.listing;
                  return RefreshIndicator(
                    onRefresh: () async {
                      return Future.delayed(Duration(seconds: 1), () {
                        setState(() {
                          context
                              .read<ItemBloc>()
                              .add(GetItem(id: id, token: token));
                        });
                        Fluttertoast.showToast(
                          msg: "Refreshing...",
                          toastLength: Toast.LENGTH_SHORT,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.black,
                          textColor: Colors.white,
                          fontSize: 16.0,
                        );
                      });
                    },
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: state.item.listing.length,
                      itemBuilder: (context, i) {
                        return ItemCardWidget(item: list[i]);
                      },
                    ),
                  );
                } else {
                  return Center(child: Text("Something went wrong!"));
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}
