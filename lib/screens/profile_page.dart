// ignore_for_file: use_build_context_synchronously

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login_page.dart';
// ignore_for_file: prefer_const_constructors

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String email = "";
  String url = "";

  @override
  void initState() {
    getUserData();
    Fluttertoast.showToast(
      msg: "Pull down to refresh",
      toastLength: Toast.LENGTH_SHORT,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 16.0,
    );
    super.initState();
  }

  getUserData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    email = prefs.getString('email')!;
    url = prefs.getString('picture')!;
  }

  confirmLogoutDialog(context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Confirm logout?"),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text(
                  'Cancel',
                  style: TextStyle(color: Colors.grey),
                )),
            TextButton(
                onPressed: () => logoutFb(), child: const Text('Confirm')),
          ],
        );
      },
    );
  }

  logoutFb() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('email');
    prefs.remove('picture');

    await FacebookAuth.instance.logOut();

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => const LoginPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF926A24),
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        automaticallyImplyLeading: false,
        title: const Text("Profile"),
        actions: [
          IconButton(
              onPressed: () => confirmLogoutDialog(context),
              icon: Icon(Icons.logout)),
          const SizedBox(
            width: 5,
          )
        ],
      ),
      body: SafeArea(
        child: WillPopScope(
          onWillPop: () => confirmLogoutDialog(context),
          child: RefreshIndicator(
            onRefresh: () async {
              return Future.delayed(Duration(seconds: 1), () {
                setState(() {
                  getUserData();
                });
                Fluttertoast.showToast(
                  msg: "Refreshing...",
                  toastLength: Toast.LENGTH_SHORT,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: 16.0,
                );
              });
            },
            child: CustomScrollView(slivers: [
              SliverFillRemaining(
                child: Center(
                  child: Column(
                    children: [
                      const SizedBox(height: 10),
                      CircleAvatar(
                          radius: 80,
                          child: ClipOval(
                            child: CachedNetworkImage(
                              height: 160,
                              width: 160,
                              fit: BoxFit.cover,
                              imageUrl: url,
                              placeholder: (context, url) => Image.asset(
                                'assets/images/no_picture.jpg',
                                fit: BoxFit.cover,
                                width: 160,
                                height: 160,
                              ),
                              errorWidget: (context, url, error) =>
                                  const Icon(Icons.error_outline),
                            ),
                          )),
                      const SizedBox(height: 50),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        padding:
                            EdgeInsets.symmetric(horizontal: 7, vertical: 15),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircleAvatar(
                              radius: 20,
                              child: Icon(
                                Icons.email_rounded,
                              ),
                            ),
                            SizedBox(width: 20),
                            Text(
                              email,
                              style: TextStyle(),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ]),
          ),
        ),
      ),
    );
  }
}
