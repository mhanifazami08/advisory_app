import 'package:dio/dio.dart';
import 'dart:convert';

import 'package:http/http.dart';

class HttpProvider {
  static const String api = "http://interview.advisoryapps.com/index.php/";

  static Future<dynamic> postHttp(
      String path, Map<String, dynamic> body) async {
    var response = await post(Uri.parse(api + path), body: body);
    return response.body;
  }

  static Future<dynamic> getHttp(String path, body) async {
    var response =
        await get(Uri.parse(api + path).replace(queryParameters: body));
    return response.body;
  }
}
